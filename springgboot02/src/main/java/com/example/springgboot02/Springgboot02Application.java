package com.example.springgboot02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springgboot02Application {

    public static void main(String[] args) {
        SpringApplication.run(Springgboot02Application.class, args);
    }

}
